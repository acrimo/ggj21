/// <reference path='../phaser.d.ts'/>

import { GameScene, GameState } from "../scenes/game-scene";

export class Player {
    public sprite: Phaser.Physics.Arcade.Sprite;

    private keyD: Phaser.Input.Keyboard.Key;
    private keyA: Phaser.Input.Keyboard.Key;
    private keyW: Phaser.Input.Keyboard.Key;
    private keyS: Phaser.Input.Keyboard.Key;

    private cursors: Phaser.Input.Keyboard.CursorKeys;

    public constructor(scene: GameScene) {
        this.sprite = scene.physics.add.sprite(scene.cameras.main.centerX,
            scene.cameras.main.centerY, "player");

        this.keyD = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
        this.keyA = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
        this.keyW = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
        this.keyS = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);

        this.cursors = scene.input.keyboard.createCursorKeys();
    }

    public update(): void {
        var x = 0;
        var y = 0;

        if (this.keyD.isDown || this.cursors.right.isDown) {
            x += 50;
        }
        if (this.keyA.isDown || this.cursors.left.isDown) {
            x -= 50;
        }
        if (this.keyW.isDown || this.cursors.up.isDown) {
            y -= 50;
        }
        if (this.keyS.isDown || this.cursors.down.isDown) {
            y += 50;
        }

        this.sprite.setVelocity(x, y);//, y);
    }
} 