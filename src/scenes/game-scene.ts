/// <reference path='../phaser.d.ts'/>

import { Game } from "phaser";
import { Player } from "../objects/player"

export enum GameState {
    INIT,
    PLAY,
    FINISH,
    LOOSE
}

export class GameScene extends Phaser.Scene {
    // state machine
    public state: GameState;

    private player: Player;

    // // ship
    // private ship: Ship;
    // private pipe: Pipe;

    // private progress: Progress;
    // private lifebar: Lifebar;

    // private gravity: number;

    // private keyD: Phaser.Input.Keyboard.Key;
    // private keyA: Phaser.Input.Keyboard.Key;

    // private inmune: boolean = false;

    // private turbine: Phaser.Sound.BaseSound;

    constructor() {
        super({
            key: "GameScene"
        });
    }

    create(): void {
        this.cameras.main.setBackgroundColor(0x00ff00);

        this.player = new Player(this);

        var block = this.physics.add.staticImage(100, 100, "player").setAlpha(1);

        this.physics.add.collider(this.player.sprite, block);

        // this.pipe = new Pipe(this);
        // this.ship = new Ship(this);

        // this.progress = new Progress(this);
        // this.lifebar = new Lifebar(this);

        // this.gravity = .05;
        // this.gravity = .01;

        // this.turbine = this.sound.add("turbine-on", { loop: true, volume: 0.2 });
        // this.turbine.pause();

        // this.keyD = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
        // this.keyA = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);

        // this.input.addPointer();
        // // this.input.on("pointerdown", () => {
        // //     if (this.input.mousePointer.position.x > 540)
        // //         this.ship.RightTurbineOn();
        // //     else
        // //         this.ship.LeftTurbineOn();
        // // });

        this.cameras.main.fadeIn(500);
        this.state = GameState.PLAY;
    }

    public update(time: number, delta: number): void {
        switch (this.state) {
            case GameState.PLAY:
                    this.player.update();
            //         this.progress.update(delta);

            //         // all this shit
            //         if (this.keyD.isDown ||
            //             (this.input.pointer2.isDown && this.input.pointer2.x > 540) ||
            //             (this.input.pointer1.isDown && this.input.pointer1.x > 540)) {
            //             this.ship.RightTurbineOn();
            //             if (this.turbine.isPaused)
            //                 this.turbine.resume();
            //         }
            //         else {
            //             this.ship.RightTurbineOff();
            //             if (!this.turbine.isPaused) {
            //                 this.turbine.pause();
            //             }
            //         }
            //         if (this.keyA.isDown ||
            //             (this.input.pointer2.isDown && this.input.pointer2.x < 540) ||
            //             (this.input.pointer1.isDown && this.input.pointer1.x < 540)) {
            //             this.ship.LeftTurbineOn();
            //             if (this.turbine.isPaused)
            //                 this.turbine.resume();
            //         }
            //         else {
            //             this.ship.LeftTurbineOff();
            //             if (!(this.turbine.isPaused)) {
            //                 this.turbine.pause();
            //             }
            //         }

            //         this.ship.update(delta / 16);
            //         this.ship.vy += this.gravity * (delta / 16);

            //         if (this.ship.x < 0) this.ship.x = 0;
            //         if (this.ship.x > 1080) this.ship.x = 1080;

            //         if (this.ship.y > 2000) {
            //             for (let i = 0; i < 3; i++)
            //                 this.lifebar.lifes[i].setTint(0x0000FF);

            //             this.loose();
            //         }

            //         this.pipe.update(delta);

            //         let inside = this.pipe.checkInside(this.ship.x, this.ship.y, this.ship.angle);

            //         if (!inside && !this.inmune) {
            //             this.sound.play("crash-" + Math.round(Math.random()), { volume: 0.5 });
            //             if (!this.lifebar.takeHit()) {
            //                 this.inmune = true;
            //                 this.tweens.add({
            //                     targets: this.ship,
            //                     alpha: 0.2,
            //                     yoyo: true,
            //                     repeat: 5,
            //                     duration: 200,
            //                     onComplete: () => {
            //                         this.inmune = false;
            //                     }
            //                 });
            //             }
            //             else {
            //                 this.loose();
            //             }
            //         }
        }

    }

    private loadMenu() {
        // fade screen
        this.cameras.main.fadeOut(500);

        // after fade animation start menu scene
        this.time.delayedCall(500, this.scene.start,
            ["LoadScene"], this.scene);
    }

    private restart() {
        // fade screen
        this.cameras.main.fadeOut(500);

        // after fade animation start menu scene
        this.time.delayedCall(500, this.scene.start,
            ["GameScene"], this.scene);
    }
}