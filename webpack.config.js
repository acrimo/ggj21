var path = require('path');

// phaser webpack config
var phaserModule = path.join(__dirname, '/node_modules/phaser/')
var phaser = path.join(phaserModule, 'src/phaser.js')

// single file export
module.exports = {
  entry: './src/game.ts',
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'game.js',
  },
  module: {
    rules: [
      { test: /\.ts$/, loader: 'ts-loader', exclude: '/node_modules/' }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      phaser: phaser
    }
  }
};

// multiple
// module.exports = {
//   entry: {
//     '1/game': path.resolve(__dirname, './src/1/game.ts'),
//     '2/game': path.resolve(__dirname, './src/2/game.ts'),
//     '3/game': path.resolve(__dirname, './src/3/game.ts'),
//     '5/game': path.resolve(__dirname, './src/5/game.ts'),
//     '6/game': path.resolve(__dirname, './src/6/game.ts'),
//     '7/game': path.resolve(__dirname, './src/7/game.ts'),
//   },
//   output: {
//     path: path.resolve(__dirname, './build'),
//     filename: '[name].js',
//   },
//   module: {
//     rules: [
//       { test: /\.ts$/, loader: 'ts-loader', exclude: '/node_modules/' }
//     ]
//   },
//   resolve: {
//     extensions: ['.ts', '.js'],
//     alias: {
//       phaser: phaser
//     }
//   }
// };
